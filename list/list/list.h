#pragma once
#include<iostream>
#include<string.h>
#include<assert.h>
#include"reverse_iterator.h"
using namespace std;
namespace wzd
{
	template<class T>
	struct ListNode
	{
		ListNode<T>* _next;
		ListNode<T>* _prev;
		T _data;
		
		ListNode(const T& data = T())
			:_next(nullptr)
			, _prev(nullptr)
			, _data(data)
		{
		}
	};
	template<class T,class Ref,class Ptr>
	struct _list_iterator
	{
		typedef ListNode<T> Node;
		typedef _list_iterator<T,Ref,Ptr> self;
		
		typedef Ref reference;
		typedef Ptr pointer;

		Node* _node;
		_list_iterator(Node* x)
			:_node(x)
		{}
		Ref operator*()
		{
			return _node->_data;
		}
		Ptr operator->()
		{
			&_node->_data;
		}
		//前置++
		self&  operator++()
		{
			_node = _node->_next;
			return *this;
		}
		//后置++
		self operator++(int)
		{
			self tmp(*this);
			_node = _node->_next;
			return tmp;
		}
		//前置--
		self&  operator--()
		{
			_node = _node->_prev;
			return *this;
		}
		//后置--
		self  operator--(int)
		{
			__list_iterator<T> tmp(*this);
			_node = _node->prev;
			return tmp;
		}
		bool operator!=(const self& tmp)const
		{
			return _node != tmp._node;
		}
		bool operator ==(const self& tmp)const
		{
			return _node == tmp._node;
		}
	};
	template<class T>
	class list
	{
		typedef ListNode<T> Node;
	public:
		typedef _list_iterator<T,T&,T*> iterator;
		typedef _list_iterator<T,const T&,const T*> const_iterator;


		//typedef reverse_iterator<const_iterator, const T&, const T*> const_reverse_iterator;
		typedef reverse_iterator<iterator, T&, T*> reverse_iterator;

		/*typedef reverse_iterator<const_iterator> const_reverse_iterator;
		typedef reverse_iterator<iterator> reverse_iterator;*/

		reverse_iterator rbegin()
		{
			return reverse_iterator(end());
		}
		reverse_iterator rend()
		{
			return reverse_iterator(begin());
		}
		iterator begin()
		{
			return iterator(_head->_next);
		}
		const_iterator begin()const
		{
			return const_iterator(_head->_next);
		}
		iterator end()
		{
			return iterator(_head);
		}
		const_iterator end()const
		{
			return const_iterator(_head->_prev);
		}
		list()
		{
			//设置哨兵位
			_head = new Node();
			_head->_next = _head;
			_head->_prev = _head;
		}
		//list<Date> lt1(5,Date(2022,3,15)
		//list<int> lt2(5,1)
		//这里需要注意的是可能构造函数在匹配的时候会和下面的
		//拥有函数模板的构造函数匹配所有这里做出处理，让编译器取匹配更合适的哪一个构造函数
		list(int n, const T& val = T())
		{
			_head = new Node();
			_head->_next = _head;
			_head->_prev = _head;

			for (int i = 0; i < n; i++)
			{
				push_back(val);
			}
		}

		list(size_t n, const T& val = T())
		{
			_head = new Node();
			_head->_next = _head;
			_head->_prev = _head;

			foe(size_t i = 0; i < n; ++i)
			{
				push_back(val);
			}
		}
		template<class Inputiterator>
		list(Inputiterator frist, Inputiterator last)
		{
			_head = new Node();
			_head->_next = _head;
			_head->_prev = _head;

			while (frist != last)
			{
				push_back(*frist);
				++frist;
			}
		}
		//现代写法
		//lt(lt1)
		list(const list<T>& lt)
		{
			_head = new Node();
			_head->_next = _head;
			_head->_prev = _head;

			list<T> tmp(li.begin().lt.end());

			std::swap(_head,tmp._head);
		}
		//lt2 = lt1
		list<T>& operator=(list<T> lt)
		{
			std::swap(_head, lt._head);
			return *this;
		}
		//传统写法
		////构造函数
		////lt(l1)
		//list(const list<T>& lt)
		//{
		//	_head = new Node();
		//	_head->_prev = _head;
		//	_head->_next = _head;
		//	while (auto e: lt)
		//	{
		//		push_back(e);
		//	}
		//}
		////lt2 = lt1
		//list<T>& operator=(const list<T>& lt)
		//{
		//	if (this != &lt)
		//	{
		//		clear();
		//		for (auto e : lt)
		//		{
		//			push_back(e);
		//		}
		//	}
		//}
		~list()
		{
			clear();

			delete _head;
			_head = nullptr;
		}
		void clear()
		{
			//传统写法
			/*iterator it = begin();
			while (it != end())
			{
				iterator del = i++;
				delete del._node;
			}
			_head->_next = _head;
			_head->_prev = _head;*/
			//复用写法
			iterator it = begin();
			while (it != end())
			{
				erase(it++);
			}
		}
		void push_front(const T& x)
		{
			insert(begin(), x);
		}
		void pop_back()
		{
			erase(end());
		}
		
		void pop_front()
		{
			erase(begin());
		}
		iterator insert(iterator pos,const T& x)
		{
			Node* cur = pos._node;
			Node* prev = cur->_prev;
			Node* newnode = new Node(x);
			Node* next = cur->_next;

			delete cur;
			prev->_next = newnode;
			newnode->_prev = prev;
			newnode->_next = cur;
			cur->_prev = newnode;

			return iterator(newnode);

		}
		iterator erase(iterator pos)
		{
			assert(pos != end());
			
			Node* prev = pos._node->_prev;
			Node* next = pos._node->_next;

			delete pos._node;
			
			prev->_next = next;
			next->_prev = prev;

			return iterator(next);
		}
		void push_back(const T& x)
		{
			Node* tail = _head->_prev;
			Node* newnode = new Node(x);

			tail->_next = newnode;
			newnode->_prev = tail;
			newnode->_next = _head;
			_head->_prev = newnode;
		}
	private:
		Node* _head;
	};

	void test_list1()
	{
		list<int> lt;
		lt.push_back(2);
		lt.push_back(3);
		lt.push_back(4);
		lt.push_back(5);
		lt.push_back(6);

		//list<int>::iterator it = lt.begin();
		//while(it != lt.end())
		//{
		//	*it *= 2;
		//	cout << *it << endl;
		//}
		for (auto e : lt)
		{
			cout << e << " ";
			/*cout << typeid(e).name();*/
		}
		cout << endl;
	}
	void test_list2()
	{
		list<int> lt;
		lt.push_back(2);
		lt.push_back(3);
		lt.push_back(4);
		lt.push_back(5);
		lt.push_back(6);

		list<int>::reverse_iterator it = lt.rbegin();
		while (it != lt.rend())
		{
			cout << *it << " ";
			++it;
		}
		cout << endl;
	}
}