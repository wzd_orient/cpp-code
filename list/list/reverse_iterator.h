#pragma once

namespace wzd
{
	template<class Iterator,class Ref,class Ptr>
	//template <class Iterator>
	//适配器
	class reverse_iterator
	{
		typedef  reverse_iterator<Iterator, Ref, Ptr> self;

		//typedef reverse_iterator<Iterator> self;

	public:
		reverse_iterator(Iterator it)
			:_it(it)
		{
		}
		//typename是只有在模板实例化之后才辨别出类型
		//typename Iterator::reference operator*()
		//{
		//	//return *_it;
		//	Iterator prev = _it;
		//	return *--prev;
		//}

		/*typename Iterator::pointer operator->()
		{
			return &operator*();
		}*/

		Ref operator*()
		{
			Iterator prev = _it;
			return *--prev;
		}
		Ptr operator->()
		{
			return &operator*();
		}
		
		self operator++()
		{
			--_it;
			return *this;
		}

		self& operator--()
		{
			++_it;
			return *this;
		}
		bool operator!=(const self& rit)const
		{
			return _it != rit._it;
		}

	private:
		Iterator _it;
	};
}