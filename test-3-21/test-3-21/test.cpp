#define _CRT_SECURE_NO_WARNINGS 1
#include<stdlib.h>
#include<vector>
#include<iostream>
using namespace std;
//class Solution {
//public:
//	int MoreThanHalfNum_Solution(vector<int> numbers) {
//		sort(numbers.begin(), numbers.end());
//		int count = 0;
//		int min = 0;
//		if (numbers.size() == 1)
//		{
//			return numbers[0];
//		}
//		if (numbers.size() & 1)
//		{
//			min = numbers.size() / 2 + 1;
//		}
//		else
//		{
//			min = numbers.size() / 2;
//		}
//		for (int i = 0; i < numbers.size(); i++)
//		{
//			if (numbers[min] == numbers[i])
//			{
//				count++;
//			}
//		}
//		if (count > min)
//		{
//			return numbers[min];
//		}
//		return -1;
//	}
//};
class Solution {
public:
	void replaceSpace(char *str, int length) {
		int old_length = length;
		int new_length = 0;
		int count = 0;
		const char* start = str;
		while (*start)
		{
			if (*start == ' ')
			{
				count++;
			}
			start++;
		}
		new_length = length + count * 2;
		char* tmp = new char[new_length + 1];
		char* end = str + length;
		while (end >= str)
		{
			if (*end != ' ')
			{
				tmp[new_length--] = *end;
			}
			else
			{
				tmp[new_length--] = '0';
				tmp[new_length--] = '2';
				tmp[new_length--] = '%';
			}
			end--;
		}
		str = tmp;
		tmp = nullptr;
		cout << str << endl;
	}
};
int main()
{
	char arr[] = "hello word";
	Solution s1;
	s1.replaceSpace(arr, strlen(arr));
	return 0;
}
      