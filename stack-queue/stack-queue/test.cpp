#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include <stack>
#include<queue>
using namespace std;

#include "Stack.h"
#include"queue.h"
#include"priority_queue.h"

//void test_stack()
//{
//	stack<int> s;
//	s.push(1);
//	s.push(2);
//	s.push(3);
//	s.push(4);
//
//	while (!s.empty())
//	{
//		cout << s.top() << " ";
//		s.pop();
//	}
//	cout << endl;
//}

//void test_queue()
//{
//	queue<int> q;
//	q.push(1);
//	q.push(2);
//	q.push(3);
//	q.push(4);
//
//	while (!q.empty())
//	{
//		cout << q.front() << " ";
//		q.pop();
//	}
//	cout << endl;
//}
struct LessPDate
{
	bool operator()(const Date* d1, const Date* d2) const
	{
		//return *d1 < *d2;
		return (d1->_year < d2->_year) ||
			(d1->_year == d2->_year && d1->_month < d2->_month) ||
			(d1->_year == d2->_year && d1->_month == d2->_month && d1->_day < d2->_day);
	}
};
void test_priority_queue2()
{
	// 如果数据类型，不支持比较，或者比较的方式，不是你想要的
	// 那么可以自己实现仿函数，按照自己想要的方式去比较，控制比较逻辑
	void test_priority_queue2()
	{
		priority_queue<Date*, vector<Date*>, LessPDate> pq;

		pq.push(new Date(2022, 3, 26));
		pq.push(new Date(2021, 10, 26));
		pq.push(new Date(2023, 3, 26));

		while (!pq.empty())
		{
			cout << *pq.top();
			pq.pop();
		}
		cout << endl;
	}
}
int main()
{
	//test_stack();
	//test_queue();

	//wzd::test_stack();
	//wzd::test_queue();
	wzd::test_priority_queue();
	return 0;
}
