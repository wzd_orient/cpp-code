#pragma once
#include<vector>
#include<list>
#include<forward_list>
using namespace std;
//适配器模式
namespace wzd
{
	template<class T,class Container = deque<T>>
	class stack
	{
	public:
		bool empty()const
		{
			return _con.empty();
		}
		size_t size()const
		{
			return _con.size();
		}
		const T& top()const
		{
			return _con.back();
		}
		void pop()
		{
			_con.pop_back();
		}
		void push(const T& x)
		{
			_con.push_back(x);
		}
	private:
		Container _con;
	};

	void test_stack()
	{
		//后进先出 -- 不支持迭代器
		stack<int, std::vector<int>> s;
		//stack<int> s;
		s.push(1);
		s.push(2);
		s.push(3);
		s.push(4);
		while (!s.empty())
		{
			cout << s.top() << " ";
			s.pop();
		}
		cout << endl;

	}
}