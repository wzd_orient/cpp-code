#pragma once

#include<vector>
#include<iostream>
using namespace std;
class Date
{
	friend struct LessPDate;
public:
	Date(int year = 1900, int month = 1, int day = 1)
		: _year(year)
		, _month(month)
		, _day(day)
	{}

	bool operator<(const Date& d)const
	{
		return (_year < d._year) ||
			(_year == d._year && _month < d._month) ||
			(_year == d._year && _month == d._month && _day < d._day);
	}

	bool operator>(const Date& d)const
	{
		return (_year > d._year) ||
			(_year == d._year && _month > d._month) ||
			(_year == d._year && _month == d._month && _day > d._day);
	}

	friend ostream& operator<<(ostream& _cout, const Date& d);
private:
	int _year;
	int _month;
	int _day;
};
ostream& operator<<(ostream& _cout, const Date& d)
{
	_cout << d._year << "-" << d._month << "-" << d._day << endl;
	return _cout;
}

namespace wzd
{
	template<class T>
	struct Less
	{
		bool operator()(const T& x, const T& y)const
		{
			return x < y;
		}
	};
	template<class T>
	struct Greater
	{
		bool operator()(const T& x, const T& y)const
		{
			return x > y;
		}
	};
	template<class T,class Container = vector<T>,class Compare = Less<T>>
	class Priority_queue
	{
	private:
		void adjust_up(int child)
		{
			Compare com;
			int parent = (child - 1)/ 2;
			while (child > 0)
			{
				if (com(_con[parent], _con[child]))
				{
					swap(_con[parent], _con[child]);
					child = parent;
					parent = (child - 1) / 2;
				}
				else
				{
					break;
				}
				
			}
		}
		void adjust_down(int parent)
		{
			Compare com;
			int child = parent * 2 + 1;
			//child = _con[child] < _con[child + 1] ? child : child + 1;
			while (child < _con.size())
			{
				if (child + 1 < _con.size() && com(_con[child], _con[child + 1]))
				{
					++child;
				}
				if ( com(_con[parent], _con[child]))
				{
					swap(_con[parent], _con[child]);
					parent = child;
					child = parent * 2 + 1;
				}
				else
				{
					break;
				}

			}

		}
	public:
		Priority_queue()
		{}
		template<class InputIterator>
		Priority_queue(InputIterator frist, InputIterator last)
			:_con(frist,last)
		{
			//����
			for (int i = (_con.size() - 1 - 1) / 2 ; i >= 0; --i)
			{
				adjust_down(i);
			}
		}
		void push(const T& x)
		{
			_con.push_back(x);
			adjust_up(_con.size() - 1);
		}
		void pop()
		{
			swap(_con[0], _con[_con.size() - 1]);
			_con.pop_back();
			adjust_down(0);
		}
		const T& top()
		{
			return _con[0];
		}
		size_t size()
		{
			return _con.size();
		}
		bool empty()
		{
			return _con.empty();
		}
	private:
		Container _con;
	};

	void test_priority_queue()
	{
		Priority_queue<int> pq;
		pq.push(1);
		pq.push(2);
		pq.push(3);
		pq.push(4);
		while (!pq.empty())
		{
			std::cout << pq.top() << " ";
			pq.pop();
		}
		std::cout << endl;
	}
}