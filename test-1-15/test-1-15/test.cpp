#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
//展开全部的命名空间,指定的作用域
using namespace std;
//std是c++中库中的一个命名空间
using std::cout;
using std::endl;
using std::cin;
//namespace wzd
//{
//	//命名空间可以存放函数/变量
//	int rand = 10;
//	int i = 250;
//	int Add(int x, int y)
//	{
//		return x + y;
//	}
//	namespace wang
//	{
//		int str = 200;
//	}
//}
//namespace wzd
//{
//	int Mul(int left, int right)
//	{
//		return left * right;
//	}
//}
////指定展开某一个
//using wzd::i;
//struct student
//{
//	char name[20];
//	int age;
//	char sex[3];
//};
//int  func(int a = 0,int b = 5)
//{
//	int c = a + b;
//	//返回时并不会创建一个临时变量
//	return c;
//}
//int&  fun(int a = 0, int b = 5)
//{
//	int c = a + b;
//	//引用返回时不会生成一个拷贝，返回的是c的别名
//	return c;
//}
//void TestFor()
//{
//	int arr[] = { 1, 2, 3, 4, 5 };
//	for (auto& e : arr)
//		e *= 2;
//	for (auto e : arr)
//		cout << e << " ";
//	return 0;
//}
//int main()
//{/*
//	double j = 1.11;
//	const char* str = "hello world";
//	int a = 0;
//	struct student s1 = { "张三", 20, "男"};
//	cout << s1.name << " " << s1.age << " " << s1.sex << endl;*/
//	//cin >> a;
//	//cout << a << endl;
//	//cout << i << endl;
//	//cout << str << endl;
//
//	//自动识别类型
//	/*cout << j << endl;
//	cout << wzd::rand << endl;
//	cout << wzd::wang::str << endl;*/
//	//引用的问题
//	//int i = 25;
//	//int& a = i;
//	//cout << a << endl;
//	
//	//常引用权限的放大和缩小
//	//权限的缩小  - 可以
//	int b = 25;
//	const int& c = b;
//	cout << c << endl;
//	double b = 1.11;
//	int i = b;
//	//int& c = b  --- 不对
//	const int& c = b;//产生临时变量，右值具有常性，只能读不能修改
//	char* ch = nullptr;
//	//函数的重载
//	return 0;
//}
int Add(int left, int right)
{
		return left + right;
}
double Add(double left, double right)
{
	return left + right;
}
long Add(long left, long right)
{
	return left + right;
}
int main()
{
	Add(10, 20);
	Add(10.0, 20.0);
	Add(10L, 20L);
	return 0;
}