#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
using std::cout;
using std::endl;
class A
{
public:
	A(int a)
	{
		_a = a;
	}
private:
	int _a;
};
class Date
{
public:
	//static void Print()
	//{
	//	cout << "hell class" << endl;
	//}
	Date(int year, int month, int day)
		:_year(year)
		, _month(day)
		, _day(day)
	{
	}
	void Print()
	{
		cout << _year << "/" << _month << "/" << _day << endl;
	}
	//初始化列表 - 成员变量定义的地方
	//Date(int year, int month, int day,int i)
	//	:_year(year)
	//	, _month(day)
	//	, _day(day)
	//	, _ref(i)
	//	, _aa(-1)
	//{
	//}
private:
	int _year;//声明
	int _month;
	int _day;

	//const int _N = 0;//const
	//int& _ref;//引用
	//A _aa;//没有默认构造函数自定义类型成员变量
	//总结一下
	//1.初始化列表 - 成员变量定义的地方
	//2.const,引用，没有默认构造函数的自定义类型成员变量必须在初始化列表初始化，因为他们都必须在定义的时候初始化
	//3.对于想其他类型成员变量，如：int year，int month在哪里初始化都是可以的
};
int main()
{
	//Date::Print();
	int i = 0;
	//Date d1(2022, 3, 3, i);
	Date d1(2022, 3, 3);
	d1.Print();
	//Date d2 = 2022; vs2013跑步过去，vs2019好像可以跑的过去
	// 本来用2022构造一个临时对象Date(2022)，再用这个对象拷贝构造d2
	// 但是C++编译器在连续的一个过程中，多个构造会被优化，合二为一。
	// 所以这里被优化为直接就是一个构造
	//d2.Print();
	return 0;
}