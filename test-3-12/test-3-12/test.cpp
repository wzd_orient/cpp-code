#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<malloc.h>
using namespace std;
//C/C++内存管理
//int main()
//{
//	//内置类型
//	int* p1 = (int*)malloc(sizeof(int));
//	//new在堆上申请一块int大小的空间
//	int* p3 = new int;
//	//new在堆上申请一块int大小的空间并初始化为5
//	int* p4 = new int(5);
//	//new在对堆上申请一块int[5]数组大小的空间，不初始化
//	int* p5 = new int[5];
//	//c++98不支持初始化new数据，c++支持用{}列表初始化
//	int* p6 = new int[5]{1, 2, 3};
//	cout << p6[1] << endl;
//	free(p1);
//	delete p3;
//	delete p4;
//	delete[] p5;
//	delete[] p6;
//	p1 = nullptr;
//	p3 = nullptr;
//	p4 = nullptr;
//	p5 = nullptr;
//	p6 = nullptr;
//	return 0;
//}
//class A
//{
//public:
//	A(int a = 0)
//		:_a(a)
//	{
//		cout << "A(int a = 0)" << endl;
//	}
//	~A()
//	{
//		cout << "~A()" << endl;
//	}
//private:
//	int _a;
//private:
//};
//int main()
//{
//	// 动态申请单个A对象 和 5个A对象数组
//	A* p1 = (A*)malloc(sizeof(A));
//	A* p2 = (A*)malloc(sizeof(A)*5);
//
//	A* p3 = new A;
//	A* p4 = new A[5];
//
//	A* p5 = new A(10);
//	//vs2013不能跑，vs2019可以跑
//	//A* p6 = new A[5]{10, 20, 30, 40, 50};
//	free(p1);
//	free(p2);
//	//delete 先调用指针类型析构函数 + 释放内存
//	delete p3;
//	delete[] p4;
//	delete p5;
//	//delete[] p6;
//	return 0;
//}
void BuyMemory()
{
	char* p2 = new char[4];
	//p2 = new char[1024u * 1024u * 1024u * 1u];
	printf("%p\n", p2);
}
int main()
{
	// 面向对象的语言，处理错误的方式一般是抛异常，C++中也要求出错抛异常 -- try catch
	// 面向过程的语言，处理错误的方式是什么-》返回值+错误码解决
	/*char* p1 = (char*)malloc(1024u*1024u*1024u*2u);
	if (p1 == nullptr)
	{
	printf("%d\n", errno);
	perror("malloc fail");
	exit(-1);
	}
	else
	{
	printf("%p\n", p1);
	}*/

	try
	{
		BuyMemory();
	}
	catch (const exception& e)
	{
		cout << e.what() << endl;
	}

	return 0;
}