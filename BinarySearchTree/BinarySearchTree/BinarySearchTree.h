#pragma once

#include<iostream>
#include<stdlib.h>
#include<stdbool.h>

using namespace std;

namespace K
{
	template<class K>
	struct BSTreeNode
	{
		BSTreeNode(const K& key)
		:_left(nullptr)
		, _right(nullptr)
		, _key(key)
		{}

		BSTreeNode<K>* _left;
		BSTreeNode<K>* _right;
		K _key;

	};

	//递归实现
	template<class K>
	struct BSTree
	{
		typedef BSTreeNode<K> Node;
	public:
		BSTree()
			:_root(nullptr)
		{}
		bool EraseR(const K& key)
		{
			return _EraseR(_root, key);
		}
		Node* FindR(const K& key)
		{
			return _FindR(_root, key);
		}
		bool InsertR(const K& key)
		{
			return _InsertR(_root, key);
		}
		void InOrder()
		{
			_InOrder(_root);
			cout << endl;
		}
		//非递归实现
		bool Find(const K& key)
		{
			Node* cur = _root;
			while (cur)
			{
				if (cur->_right < key)
				{
					cur = cur->_right;
				}
				else if (cur->_left > key)
				{
					cur = cur->_left;
				}
				else
				{
					return true;
				}
			}
			return false;
		}
		bool Insert(const K& key)
		{
			if (_root == nullptr)
			{
				_root = new Node(key);
				return true;
			}
			Node* parent = nullptr;
			Node* cur = _root;
			while (cur)
			{
				if (cur->_key < key)
				{
					parent = cur;
					cur = cur->_right;
				}
				else if (cur->_key > key)
				{
					parent = cur;
					cur = cur->_left;
				}
				else
				{
					return false;
				}
			}
			//结束了，就是已经找到可以插入节点的位置
			//parent = new Node(key);
			//这个地方一定要注意不能立即插入需要判断大小，然后再考虑是插入左右那个子节点
			if (parent->_key < key)
			{
				parent->_right = new Node(key);
			}
			else
			{
				parent->_left = new Node(key);
			}
			return true;
		}
		bool Erase(K key)
		{
			Node* parent = nullptr;
			Node* cur = _root;
			while (cur)
			{
				if (cur->_key < key)
				{
					parent = cur;
					cur = cur->_right;
				}
				else if (cur->_key > key)
				{
					parent = cur;
					cur = cur->_left;
				}
				else
				{
					//自己写的第一种方法，傻傻的去交换节点
					//而且思路也错了
					//if (parent == nullptr)
					//{
					//	Node* minparent = nullptr;
					//	Node* min = cur->_right;
					//	while (min->_left != nullptr)
					//	{
					//		minparent = min;
					//		min = min->_left;
					//	}
					//	minparent->_left = nullptr;

					//	min->_left = cur->_left;
					//	min->_right = cur->_right;
					//	delete cur;
					//	_root = min;
					//	break;
					//}
					//if (parent->_right == cur)
					//{
					//	if (cur->_left == nullptr)
					//	{
					//		parent->_right = cur->_right;
					//	}
					//	else if (cur->_right == nullptr)
					//	{
					//		parent->_right = cur->_left;
					//	}
					//	else
					//	{
					//		//如果不是cur有两个子孩子的话就需要找到
					//		//这里找右子树的最小节点

					//		//自己写的有bug不能只使用一个指针，因为无法删除找到的最小节点的父节点指向的指针
					//		//还是得定义两个指针
					//		/*Node* min = cur;
					//		while (min->_left != nullptr)
					//		{
					//			min = min->_left;
					//		}
					//		parent->_right = min;
					//		if (cur->_left == min)
					//		{
					//			cur->_left = nullptr;
					//		}
					//		min->_left = cur->_left;
					//		min->_right = cur->_right;
					//		delete  cur;*/


					//		//直接写得第二种尝试，感觉自己想一个傻B一样为啥不直接把值给交换了，还在那里傻傻的挪动节点
					//		//真的好傻呀，好呆！！！
					//		Node* minparent = nullptr;
					//		Node* min = cur;
					//		while (min->_left != nullptr)
					//		{
					//			minparent = min;
					//			min = min->_left;
					//		}
					//		minparent->_left = nullptr;

					//		min->_left = cur->_left;
					//		min->_right = cur->_right;
					//		parent->_right = min;
					//		delete cur;
					//	}
					//	return true;
					//}
					//else
					//{
					//	if (cur->_left == nullptr)
					//	{
					//		parent->_left = cur->_right;
					//	}
					//	else if (cur->_right == nullptr)
					//	{
					//		parent->_left = cur->_left;
					//	}
					//	else
					//	{
					//		Node* maxparent = nullptr;
					//		Node* max = cur;
					//		while (max->_right != nullptr)
					//		{
					//			maxparent = max;
					//			max = max->_right;
					//		}
					//		maxparent->_right = nullptr;

					//		max->_left = cur->_left;
					//		max->_right = cur->_right;
					//		parent->_left = max;
					//		delete cur;
					//	}
					//	return true;
					//}

					//第二种方法：直接去交换节点的值它不香吗？？？
					if (cur->_left == nullptr)
					{
						if (parent == nullptr)
						{
							_root = cur->_right;
						}
						else
						{
							if (parent->_left == cur)
								parent->_left = cur->_right;
							else
								parent->_right = cur->_right;
						}

						delete cur;
					}
					else if (cur->_right == nullptr)
					{
						if (parent == nullptr)
						{
							_root = cur->_left;
						}
						else
						{
							if (parent->_left == cur)
								parent->_left = cur->_left;
							else
								parent->_right = cur->_left;
						}
						delete cur;
					}
					else
					{
						Node* minparent = cur;//这里给的是cur而不是nullptr这是很妙的一种方法
						Node* min = cur->_right;
						while (min->_left)
						{
							minparent = min;
							min = min->_left;
						}
						cur->_key = min->_key;
						//删除5的情况
						if (minparent->_left == min)
						{
							minparent->_left = min->_right;
						}
						//删除7的情况
						else
						{
							minparent->_right = min->_right;
						}
						delete min;
					}
					return true;
				}
			}
			return false;
		}
private:
		bool _EraseR(Node*& root, const K& key)
		{
			if (root == nullptr)
			{
				return false;
			}
			if (root->_key < key)
			{
				return _EraseR(root->_right, key);
			}
			else if (root->_key > key)
			{
				return _EraseR(root->_left, key);
			}
			else
			{
				Node* tmp = root;
				if (root->_left == nullptr)
				{
					root = root->_right;
				}
				else if (root->_right == nullptr)
				{
					root = root->_left;
				}
				else
				{
					Node* min = root->_right;
					while (min->_left)
					{
						min = min->_left;
					}
					swap(root->_key, min->_key);
					return _EraseR(root->_right, key);
				}
				delete tmp;
				return true;
			}
		}
		Node* _FindR(Node* root, const K& key)
		{
			if (root == nullptr)
			{
				return nullptr;
			}
			if (root->_key < key)
				_FindR(root->_right, key);
			else if (root->_key > key)
				_FindR(root->_left, key);
			else
			{
				return root;
			}
		}
		bool _InsertR(Node*& root, const K& key)
		{
			if (root == nullptr)
			{
				root = new Node(key);
				return true;
			}
			if (root->_key < key)
				_InsertR(root->_right, key);
			else if (root->_key > key)
				_InsertR(root->_left, key);
			else
			{
				return true;
			}
			return false;
		}
		void _InOrder(Node* root)
		{
			if (root == nullptr)
			{
				return;
			}
			_InOrder(root->_left);
			cout << root->_key << " ";
			_InOrder(root->_right);
		}
	private:
		Node* _root;
	};
}

namespace KV
{
	template<class K, class V>
	struct BSTreeNode
	{
		BSTreeNode(const K& key, const V& value)
		:_left(nullptr)
		, _right(nullptr)
		, _key(key)
		, _value(value)
		{}

		BSTreeNode<K, V>* _left;
		BSTreeNode<K, V>* _right;
		K _key;
		V _value;

	};

	//递归实现
	template<class K, class V>
	struct BSTree
	{
		typedef BSTreeNode<K, V> Node;
	public:
		BSTree()
			:_root(nullptr)
		{}
		bool EraseR(const K& key, const V& value)
		{
			return _EraseR(_root, key, value);
		}
		Node* FindR(const K& key)
		{
			return _FindR(_root, key);
		}
		bool InsertR(const K& key, const V& value)
		{
			return _InsertR(_root, key, value);
		}
		void InOrder()
		{
			_InOrder(_root);
			cout << endl;
		}
	private:
		bool _EraseR(Node*& root, const K& key, const V& value)
		{
			if (root == nullptr)
			{
				return false;
			}
			if (root->_key < key)
			{
				return  _EraseR(root->_right, key, value);
			}
			else if (root->_key > key)
			{
				return  _EraseR(root->_right, key, value);
			}
			else
			{
				Node* tmp = root;
				if (root->_left == nullptr)
				{
					root = root->_right;
				}
				else if (root->_right == nullptr)
				{
					root = root->_left;
				}
				else
				{
					Node* min = root->_right;
					while (min->_left)
					{
						min = min->_left;
					}
					swap(root->_key, min->_key);
					swap(root->_value, min->_value);
					return _EraseR(root->_right, key, value);
				}
				delete tmp;
				return true;
			}
		}
		Node* _FindR(Node* root, const K& key)
		{
			if (root == nullptr)
			{
				return nullptr;
			}
			if (root->_key < key)
				_FindR(root->_right, key);
			else if (root->_key > key)
				_FindR(root->_left, key);
			else
			{
				return root;
			}
		}
		bool _InsertR(Node*& root, const K& key, const V& value)
		{
			if (root == nullptr)
			{
				root = new Node(key, value);
				return true;
			}
			if (root->_key < key)
				_InsertR(root->_right, key,value);
			else if (root->_key > key)
				_InsertR(root->_left, key,value);
			else
			{
				return true;
			}
			return false;
		}
		void _InOrder(Node* root)
		{
			if (root == nullptr)
			{
				return;
			}
			_InOrder(root->_left);
			cout << root->_key << " " << root->_value << " ";
			_InOrder(root->_right);
		}
	private:
		Node* _root;
	};
}