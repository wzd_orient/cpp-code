#define _CRT_SECURE_NO_WARNINGS 1
#include<string>
#include"BinarySearchTree.h"

void TestBSTree1()
{
	// 字典KV模型
	KV::BSTree<string, string> dict;
	dict.InsertR("sort", "排序");
	dict.InsertR("left", "左边");
	dict.InsertR("right", "右边");
	dict.InsertR("map", "地图、映射");
	//...

	string str;
	while (cin >> str)
	{
		//KV::BSTreeNode<string, string>* ret = dict.FindR(str);
		auto ret = dict.FindR(str);
		if (ret)
		{
			cout << "对应中文解释：" << ret->_value << endl;
		}
		else
		{
			cout << "无此单词" << endl;
		}
	}
}
void TestBSTree2()
{
	// 统计水果出现次数
	string arr[] = { "苹果", "西瓜", "草莓", "苹果", "西瓜", "苹果", "苹果", "西瓜", "苹果", "香蕉", "苹果", "香蕉" };
	KV::BSTree<string, int> countTree;
	for (auto& str : arr)
	{
		//BSTreeNode<string, int>* ret = countTree.Find(str);
		auto ret = countTree.FindR(str);
		if (ret != nullptr)
		{
			ret->_value++;
		}
		else
		{
			countTree.InsertR(str, 1);
		}
	}

	countTree.InOrder();
}
int main()
{
	//TestBSTree1();
	TestBSTree2();
	//KV::BSTree<string,string> st;
	
	//int arr[] = {5,3,1,4,0,2,7,6,8,9,5};
	//K::BSTree<int> st;
	//for (auto e : arr)
	//{
	//	st.InsertR(e);
	//}
	////二插搜索树具有排序和去重的双重效果
	////st.InOrder();
	//st.InOrder();
	//for (auto e : arr)
	//{
	//	st.EraseR(e);
	//	st.InOrder();
	//}
	/*st.EraseR(5);
	st.InOrder();*/
	//st.EraseR(8);
	

	//st.Erase(5);
	//st.InOrder();
	return 0;
}