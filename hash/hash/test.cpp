#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<vector>
#include<map>
#include<set>
#include<unordered_map>
#include<time.h>
#include<unordered_set>
#include<stdio.h>
#include"HashTable.h"
#include"UnorderedMap.h"
#include"Unordered_set.h"
using namespace std;
void test_unordered_set()
{
	//不排序去重
	//set会自动排序
	unordered_set<string> uss;
	uss.insert("sort");
	
	unordered_set<int> us;
	us.insert(5);
	us.insert(6);
	us.insert(1);
	us.insert(2);
	us.insert(7);
	us.insert(5);
	unordered_set<int>::iterator it = us.begin();
	while(it != us.end())
	{
		cout << *it << " ";
		++it;
	}
	cout << endl;

	for (auto e : us)
	{
		cout << e << " ";
	}
	cout << endl;
	
	//不排序不去重
	unordered_multiset<int> ums;
	ums.insert(5);
	ums.insert(6);
	ums.insert(1);
	ums.insert(2);
	ums.insert(7);
	ums.insert(5);
	for (auto e : ums)
	{
		cout << e << " ";
	}
	cout << endl;

}
void test_op()
{
	int n = 10;
	vector<int> v;
	//v.reserve(10);
	v.resize(10);
	srand(time(NULL));
	for (int i = 0; i < n; i++)
	{
		v.push_back(rand());
	}
	size_t begin1 = clock();
	set<int> s;
	for (auto e : v)
	{
		s.insert(e);
	}
	size_t end1 = clock();

	size_t begin2 = clock();
	unordered_set<int> uss;
	for (auto e : v)
	{
		uss.insert(e);
	}
	size_t end2 = clock();

	cout << "set insert" << end1 - begin1 << endl;
	cout << "unordered_set" << end2 - begin2 << endl;

	size_t begin3 = clock();
	for (auto e : v)
	{
		s.find(e);
	}
	size_t end3 = clock();

	size_t begin4 = clock();
	for (auto e : v)
	{
		uss.find(e);
	}
	size_t end4 = clock();
	

	cout << "set find" << end3 - begin3 << endl;
	cout << "unordered_set find" << end4 - begin4 << endl;

	size_t begin5 = clock();
	for (auto e : v)
	{
		s.erase(e);
	}
	size_t end5 = clock();

	size_t begin6 = clock();
	for (auto e : v)
	{
		uss.erase(e);
	}
	size_t end6 = clock();

	cout << "set erase" << end3 - begin3 << endl;
	cout << "unordered_set erase" << end4 - begin4 << endl;


}
int main()
{
	//test_unordered_set();
	//test_op();
	//wzd::TestHashTable1();
	//wzd::TestHashTable2();
	//wzd::unordered_map<int, int> mymap;
	//wzd::test_unordered_map();
	wzd::test_unordered_set();
}