#pragma once
#include<vector>
#include<string>
#include<iostream>
#include<vector>
#include<map>
#include<set>
#include<unordered_map>
#include<time.h>
#include<unordered_set>
#include<stdio.h>
using namespace std;

template<class K>
struct Hash
{
	size_t operator()(const K& key)
	{
		return key;
	}
};

// 特化
template<>
struct Hash<string>
{
	size_t operator()(const string& s)
	{
		// BKDR
		size_t value = 0;
		for (auto ch : s)
		{
			value *= 31;
			value += ch;
		}
		return value;
	}
};
namespace LinkHash
{
	enum Status
	{
		EXIST,
		EMPTY,
		DELETE
	};
	template<class T>
	struct HashNode
	{
		T  _data;
		HashNode<T>* _next;

		HashNode(const T& data)
			:_data(data)
			, _next(nullptr)
		{};
	};
	template<class K, class T, class KeyOfT, class HashFunc>
	class HashTable;
	template<class K, class T, class Ref,class Ptr,class KeyOfT, class HashFunc>
	struct _HTIterator
	{
		typedef HashNode<T> Node;
		typedef _HTIterator<K, T, Ref, Ptr, KeyOfT, HashFunc> self;
		
		Node* _node;
		HashTable<K, T, KeyOfT, HashFunc>* _pht;

		_HTIterator(Node* node, HashTable<K, T, KeyOfT, HashFunc>* pht)
			:_node(node)
			, _pht(pht)
		{}

		Ref operator*()
		{
			return _node->_data;
		}

		Ptr operator->()
		{
			return &_node->_data;
		}
		self& operator++()
		{
			if (_node->_next)
			{
				_node = _node->_next;
			}
			else
			{
				KeyOfT kot;
				HashFunc hf;
				size_t index = hf(kot(_node->_data)) % _pht->_tables.size();
				++index;
			    //找下一个不为空的桶

				while (index < _pht->_tables.size())
				{
					if (_pht->_tables[index])
					{
						break;
					}
					else
					{
						++index;
					}
				}
				//表走完了，都没有找到下一个桶
				if (index == _pht->_tables.size())
				{
					_node = nullptr;
				}
				else
				{
					_node = _pht->_tables[index];
				}
			}
			return *this;
		}
		bool operator!=(const self& s) const
		{
			return _node != s._node;
		}

		bool operator==(const self& s) const
		{
			return _node == s._node;
		}
	};
	template<class K,class T,class KeyOfT,class HashFunc>
	class HashTable
	{
		typedef HashNode<T> Node;
		template<class K, class T, class Ref, class Ptr, class KeyOfT, class HashFunc>
		friend struct _HTIterator;
	public:
		typedef _HTIterator<K, T, T&, T*, KeyOfT, HashFunc> iterator;

		iterator begin()
		{
			for (size_t i = 0; i < _tables.size(); ++i)
			{
				if (_tables[i])
				{
					return iterator(_tables[i], this);
				}
			}

			return end();
		}

		iterator end()
		{
			return iterator(nullptr, this);
		}
		bool Erase(const K& key)
		{
			if (_tables.empty())
			{
				return nullptr;
			}
			HashFunc hf;
			size_t index = hf(key) % _tables.size();

			Node* prev = nullptr;
			Node* cur = _tables[index];
			KeyOfT kot;
			while (cur)
			{
				Node* next = cur->_next;
				if (kot(cur->_data) == key)
				{
					if (prev == nullptr)
					{
						//头删
						_tables[index] = next;
					}
					else//中间删除
					{
						prev->_next = next;
					}
					--_n;
					delete cur;
					return true;
				}
				else
				{
					prev = cur;
					cur = next;
				}
			}
			return false;
		}
		Node* Find(const K& key)
		{
			if (_tables.empty())
			{
				return nullptr;
			}

			HashFunc hf;
			size_t index = hf(key) % _tables.size();

			Node* cur = _tables[index];

			KeyOfT kot;

			while (cur)
			{
				if (kot(cur->_data) == key)
				{
					return cur;
				}
				else
				{
					cur = cur->_next;
				}
			}

			return nullptr;
		}
		bool Insert(const T& data)
		{
			KeyOfT kot;
			Node* ret = Find(kot(data));
			if (ret)
			{
				return false;
			}
			HashFunc hf;
			//
			if (_n == _tables.size())
			{
				//扩容
				size_t newSize = _tables.size() == 0 ? 10 : _tables.size() * 2;
				//遍历原表，将原表中的数据全部重新映射到newSize新的数组当中
				//或者套用这里写过的inset递归实现，这里就这样使用
				vector<Node*> newTables;
				newTables.resize(newSize);
				for (size_t i = 0; i < _tables.size(); i++)
				{
					Node* cur = _tables[i];
					//这里不再复用写过的insert递归实现，而是将原来的数据重新映射到新的数组
					while (cur)
					{
						Node* next = cur->_next;

						size_t index = hf(kot(cur->_data)) % newTables.size();

						//头插节点
						cur->_next = newTables[index];
						newTables[index] = cur;
						cur = next;
					}

					_tables[i] = nullptr;

				}
				_tables.swap(newTables);
			}

			size_t index = hf(kot(data)) % _tables.size();
			
			
			//头插节点
			Node* newnode = new Node(data);
			newnode->_next = _tables[index];
			_tables[index] = newnode;

			++_n;
			return true;
		}
	private:
		vector<Node*> _tables;
		size_t _n = 0;//有效个数
	};
}
