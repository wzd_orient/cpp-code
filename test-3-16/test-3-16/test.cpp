#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<string>
#include<vector>
#include<list>
using namespace std;
//void test_string1()
//{
//	string s1("hello world");
//	//遍历 + 修改
//	//方式1：下标[]
//	for (size_t i = 0; i < s1.size(); i++)
//	{
//		s1[i] += 1;
//	}
//	for (size_t i = 0; i < s1.size(); i++)
//	{
//		cout << s1[i] << " ";
//	}
//	cout << s1.size() << endl;
//	//迭代器
//	string::iterator it = s1.begin();
//	while (it != s1.end())
//	{
//		*it -= 1;
//		++it;
//	}
//	cout << endl;
//	it = s1.begin();
//	while (it != s1.end())
//	{
//		cout << *it << " ";
//		++it;
//	}
//	cout << endl;
//	//方式3：范围for，自动往后迭代，自动判断结束
//	//c++11
//	//for循环的语法糖
//	for (auto& e : s1)
//	{
//		e -= 1;
//	}
//	for (auto e : s1)
//	{
//		cout << e << " ";
//	}
//	cout << endl;
//}
//反向迭代器
//void func(const string& s)
//{
//	auto it = s.begin();
//	while (it != s.end())
//	{
//		cout << *it << " ";
//		it++;
//	}
//	cout << endl;
//	string::const_reverse_iterator rit = s.rbegin();
//	while (rit != s.rend())
//	{
//		cout << *rit << " ";
//		++rit;
//	}
//	cout << endl;
//}
//void test_string2()
//{
//	string s1("hello world");
//	//func(s1);
//	//string::const_reverse_iterator rit = s.rbegin();
//	auto rit = s1.rbegin();
//	while(rit != s1.rend())
//	{
//		cout << *rit << " ";
//		++rit;
//	}
//	cout << endl;
//}
//void TestPushBack()
//{
//	string s;
//	s.reserve(1000);//至少申请能储存1000个字符的空间
//	size_t sz = s.capacity();
//	size_t si = s.size();
//	cout << "capacity changed: " << sz << si << "\n";
//	cout << "making s grow:\n";
//	for (int i = 0; i < 1000; i++)
//	{
//		//s.push_back('c');
//		s += 'c';
//		if (sz != s.capacity())
//		{
//			sz = s.capacity();
//			cout << "capacity changed: " << sz << '\n';
//		}
//	}
//}
//void test_string3()
//{
//	string s1;
//	s1.reserve(100);
//
//	string s2;
//	s2.resize(100, 'x');
//
//	string s3("hello world");
//	s3.reserve(100);
//
//	string s4("hello world");
//	s4.resize(100, 'x');
//
//	string s5("hello world");
//	s5.resize(5);
//}
//void testsring4()
//{
//	string s("hello world");
//	cout << s << endl;
//	cout << s.c_str() << endl;
//	//c_str()返回字符串的首元素地址
//	string file("test.txt.zip");
//	//FILE* fout = fopen(file.c_str(),"w");
//
//	//要求你去出文件的后缀名
//	size_t pos = file.rfind('.');
//	if (pos != string::npos)
//	{
//		//string suffix = file.substr(pos + 1);
//		string suffix = file.substr(pos,s.size() - pos + 1);
//		cout << suffix << endl;
//	}
//	string url("http://www.cplusplus.com/reference/string/string/find/");
//	size_t pos1 = url.find(':');
//	string protocol = url.substr(0, pos1 - 0);
//	cout << protocol << endl;
//
//	size_t pos2 = url.find('/', pos1 + 3);
//	string domain = url.substr(pos1 + 3, pos2 - (pos1 + 3));
//	cout << domain << endl;
//}
//void test_sring5()
//{
//	string s("hello world");
//	s += ' ';
//	s += "!!!!";
//	cout << endl;
//	//头插 效率低  尽量少用
//	s.insert(0, 1, 'x');
//	s.insert(s.begin(), 'y');
//	s.insert(0, "test");
//	cout << s << endl;
//
//	//中间插入
//	s.insert(5,"&&&&");
//	cout << s << endl;
//}
//void test_sring6()
//{
//	//尽量少用头部和中间删除，因为需要挪动数据，效率低
//	string s("hello world");
//	s.erase(0, 1);
//	//     位置 个数
//	s.erase(s.size() - 1, 1);
//	cout << s << endl;
//
//	s.insert(5, 6,'y');
//	cout << s << endl;
//
//	s.erase(3);
//	cout << s << endl;
//}
void test_string7()
{
	int val = stoi("1234");
	//将字符串转化成整数
	cout << val << endl;

	string str = to_string(3.14);
	cout << str << endl;
}
int main()
{
	//test_string1();
	//test_string2();
	//TestPushBack();
	//test_string3();
	//testsring4();
	//test_sring5();
	//test_sring6();
	test_string7();
	return 0;
}