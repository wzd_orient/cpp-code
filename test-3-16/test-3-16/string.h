#pragma once
#include<string.h>
#include<iostream>
#include<string>
#include<vector>
#include<list>
#include<assert.h>
using namespace std;
//string类模拟增删查改的全部实现
namespace wzd
{
	class string
	{
	public:
		typedef char* iterator;
		typedef const char* const_iterator;
		const_iterator begin()const
		{
			return _str;
		}
		const_iterator end()const
		{
			return _str + _size;
		}
		iterator begin()
		{
			return _str;
		}
		iterator end()
		{
			return _str + _size;
		}
		string(const char* str = "")
			:_size(strlen(str))
			, _capacity(_size)
		{
			_str = new char[_capacity + 1];
			strcpy(_str, str);
		}
		//传统写法 -- 本本分分去完成深拷贝
		//s2(s1)
	/*	string(const string& s)
			:_size(s._size)
			, _capacity(s._capacity)
		{
			_str = new char[_capacity + 1];
			strcpy(_str, s._str);
		}*/
		//现代写法：灵活运用构造函数
		string(const string& s)
			:_str(nullptr)
			,_size(0)
			, _capacity(0)
		{
			string tmp(s._str);
			swap(_str, tmp._str);
		}
		//传统写法：赋值函数的重载
		/*string& opertor = (const string& s)
		{
			if (this != &s)
			{
				char* tmp = new char[s.capacity + 1];
				strcpy(tmp, s._str);
				delete[] _str;
				_str = tmp;
				_size = s._size;
				_capacity = s.capacity;
			}
			retunrn *this;
		}*/
		//现代写法:利用拷贝构造函数
		/*string& opertor = (const string& s)
		{
			if (this != &s)
			{
				string tmp(s);
				swap(_str, tmp._str);
			}
		}*/
		//现代写法更简单的一种方法：传参的时候实现拷贝构造
		string& operator=(string s)
		{
			swap(_str, s._str);
			return *this;
		}
		~string()
		{
			delete[] _str;
			_str = nullptr;
			_size = _capacity = 0;
		}
		const char* c_str()const
		{
			return _str;
		}
		size_t size()const
		{
			return _size;
		}
		char& operator[](size_t pos)
		{
			assert(pos < _size);
			retunr _str[pos];
		}
		const char& operator[](size_t pos)const
		{
			assert(pos < _size);
			retunr _str[pos];
		}
		void reserve(size_t n)
		{
			if (n > _capacity)
			{
				char* tmp = new char[n + 1];
				strcpy(tmp, _str);
				delete[] _str;
				_str = tmp;
				_capacity = n;
			}
		}
		void resize(size_t n, char ch = '\0')
		{
			if (n <= _size)
			{
				_size = n;
				_str[_size] = '\0';
			}
			else
			{
				if (n > _capacity)
				{
					reverse(n);
				}
				memset(_str + _size, ch, n - _size);
				_size = n;
				_str[_size] = '\0';
			}
		}
		void Push_back(char ch)
		{
			//if (_size == _capacity)
			//{
			//	//自己写的，代码有点小菜，为啥不想办法把空间交换
			//	//菜鸡
			//	/*char* tmp = new char[_capacity + 1];
			//	strcpy(tmp._str, _str)
			//	_capacity = _capacity * 2;
			//	_str = new char[_capacity + 1];
			//	strcpy(_str,tmp.str)
			//	delete[] tmp;*/
			//	reserve(_capacity == 0 ? 4 : _capacity * 2);
			//}
			//_str[_size] = ch;
			//++_size;
			//_str[_size] = '\0';
			insert(_size, ch);
		}
		void append(const char* str)
		{
			/*if (_size + strlen(str) > _capacity)
			{
				reverse(_size + strlen(str));
			}
			strcpy(_str + _size, str);
			_size = _size + strlen(str);*/
			insert(_size, str);
		}
		string& opeator += (char ch)
		{
			Push_back(ch);

			return *this;
		}
		string& opeator += (char ch)const
		{
			Push_back(ch);
			return *this;
		}
		size_t find(char ch)
		{
			for (size_t = 0; i < _size; i++)
			{
				if (ch == _str[i])
				{
					return i;
				}
			}
			return npos;
		}
		size_t find(const char* s, size_t pos = 0)
		{
			const char* ptr = strstr(_str + pos, s);
			if (ptr == nullptr)
			{
				return npos;
			}
			else
			{
				return ptr - _str;
			}
		}
		string& insert(size_t pos, char ch)
		{
			assert(pos <= _size);
			if (_size == _capacity)
			{
				reserve(_capacity == 0 ? 4 : _capacity * 2);
			}
			//头插第一个位置的时候出现问题
			//end - 1是-1的无符号整形中很大
			//越界访问
			/*size_t end = _size;
			while (end >= pos)
			{
			_str[end + 1] = _str[end];
			--end;//pos是o这个地方end--是-1无穷大完蛋了！！！
			}*/
			/*int end = _size;
			while (end >= (int)pos)
			{
			_str[end + 1] = _str[end];
			--end;
			}*/
			size end = _size + 1;
			while (end > pos)
			{
				_str[end] = _str[end - 1];
				--end;
			}
			_str[pos] = ch;
			++_size;
			return *this;
		}
		string& insert(size_t pos, const char* s)
		{
			assert(pos <= _size);
			size_t len = strlen(s);
			if (_size + len > _capacity)
			{
				reverse(_size + len);
			}
			size_t end = _size + len;
			whill(end > pos)
			{
				_str[end] = _str[end - len];
				--end;
			}
			strncpy(_str + pos, s, len);
			return *this;
		}
		string& erase(size_t pos = 0, size_t len = npos)
		{
			assert(pos < _size);
			if (len == npos || pos + len >= _size)
			{
				_str[pos] = '\0';
				_size = pos;
			}
			else
			{
				strcp(_str + pos, _str + pos + len);
				_size -= len;
			}
			return *this;
		}
	private:
		char* _str;
		size_t _size;
		size_t _capacity;
		static const int npos;
	};
}
wzd::string::npos = -1;