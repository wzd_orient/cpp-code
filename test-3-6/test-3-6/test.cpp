#define _CRT_SECURE_NO_WARNINGS 1
//#include<iostream>
//using namespace std;
//int GetMonthDay(int year, int month)
//{
//	static int monthArray[13] = { 0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
//	int day = monthArray[month];
//	//一年其实是365天 + 5个小时
//	if (month == 2 && ((year % 4 == 0 && year % 100 != 0)
//		|| year % 400 == 0))
//	{
//		day += 1;
//	}
//	return day;
//}
//void Getmonth(int& year1, int& month1, int& day1, int day2)
//{
//	while (day2 > GetMonthDay(year1, month1))
//	{
//		day2 -= GetMonthDay(year1, month1);
//		month1++;
//	}
//	day1 += day2;
//}
//int main()
//{
//	int _year = 0;
//	int _month = 0;
//	int _day = 0;
//	int day2 = 0;
//	cin >> _year >> _month >> _day >> day2;
//	Getmonth(_year, _month, _day, day2);
//	cout << _year << "-" << _month << "-" << _day << endl;
//	return 0;
//}
//#include<iostream>
//using namespace std;
//int GetMonthDay(int year, int month)
//{
//	static int monthArray[13] = { 0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
//	int day = monthArray[month];
//	//一年其实是365天 + 5个小时
//	if (month == 2 && ((year % 4 == 0 && year % 100 != 0)
//		|| year % 400 == 0))
//	{
//		day += 1;
//	}
//	return day;
//}
//int Getday1(int year1, int month1, int day)
//{
//	while (--month1)
//	{
//		day += GetMonthDay(year1, month1);
//	}
//	return day;
//}
//int main()
//{
//	int _year;
//	int _month;
//	int _day;
//	int Getday;
//	cin >> _year >> _month >> _day;
//	Getday = Getday1(_year, _month, _day);
//	cout << Getday << endl;
//	return 0;
//}
//#include<iostream>
//using namespace std;
//int GetMonthDay(int year, int month)
//{
//	static int monthArray[13] = { 0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
//	int day = monthArray[month];
//	//一年其实是365天 + 5个小时
//	if (month == 2 && ((year % 4 == 0 && year % 100 != 0)
//		|| year % 400 == 0))
//	{
//		day += 1;
//	}
//	return day;
//}
//int GetYearMOnthDay(int& year1,int& day1)
//{
//	int i = 1;
//	int month2 = 1;
//	while (day1 > GetMonthDay(year1, i))
//	{
//		day1 -= GetMonthDay(year1, i);
//		i++;
//		month2++;
//		if (month2 == 13)
//		{
//			year1++;
//			month2 = 1;
//		}
//	}
//	return month2;
//}
//int main()
//{
//	int _year;
//	int _month;
//	int _day;
//	cin >> _year >> _day;
//	_month = GetYearMOnthDay(_year,_day);
//	printf("%04d-%02d-%02d", _year, _month, _day);
//	return 0;
//}
//class Sum{
//	friend class Solution;
//	Sum()
//		:_sum(0)
//	{}
//	int NumberSum(int n = 0)
//	{
//		while (n)//n--
//		{
//			_sum += n;
//			n--;
//		}
//		while (n--)
//		{
//			_sum += n;
//		}
//		return _sum;
//	}
//private:
//	int _sum;
//};
//class Solution{
//public:
//	int Sum_Solution(int n) {
//		return  _t.NumberSum(n);
//	}
//private:
//	Sum _t;
//};
//int main()
//{
//	Solution s1;
//	int i = s1.Sum_Solution(1);
//	return 0;
//}
//#include<iostream>
//using namespace std;
//int GetDAy(const int& date1, const int& date2)
//{
//	int max = date1;
//	int min = date2;
//	int flag = 1;
//	if (date1 < date2)
//	{
//		max = date2;
//		min = date1;
//		flag = -1;
//	}
//	int count = 0;
//	while (max != min)
//	{
//		count++;
//		min++;
//	}
//	return count * flag;
//}
//int main()
//{
//	int date1 = 0;
//	int date2 = 0;
//	cin >> date1 >> date2;
//	cout << GetDAy(date1, date2) << endl;
//	return 0;
//}
#include <cstdio>
#include <algorithm>
using namespace std;
int hash[2][12] = { { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 },
{ 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 } };

int is_leap(int year)
{
	return (year % 4 == 0 && year % 100 != 0) || year % 400 == 0;
}

int conv_day(int y, int m, int d)
{
	int sum = 0;
	y = is_leap(y);
	for (int i = 0; i < m - 1; i++)
	{
		sum += hash[y][i];
	}
	sum += d;
	return sum;
}

int main()
{
	int date1, date2;
	int y1, m1, d1;
	int y2, m2, d2;
	while (scanf("%d %d\n", &date1, &date2) != EOF)
	{
		if (date1 > date2)
			swap(date1, date2);
		y1 = date1 / 10000;
		m1 = (date1 % 10000) / 100;
		d1 = (date1 % 10000) % 100;
		y2 = date2 / 10000;
		m2 = (date2 % 10000) / 100;
		d2 = (date2 % 10000) % 100;
		int day1 = conv_day(y1, m1, d1);
		int day2 = conv_day(y2, m2, d2);

		if (y1 == y2)
			printf("%d\n", day2 - day1 + 1);
		else
		{
			int sum = (365 + is_leap(y1)) - day1 + 1;
			y1++;
			while (y1 < y2)
			{
				sum += (365 + is_leap(y1));
			}
			sum += day2;
			printf("%d\n", sum);
		}
	}
	return 0;
}