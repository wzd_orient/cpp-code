#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<string>
using namespace std;
//class Person
//{
//public:
//	void Print()
//	{
//		cout << "_name:" << _name << endl;
//		cout << "_age:" << _age << endl;
//	}
//protected:
//	string _name = "peter";
//	int _age = 18;
//private:
//	int _aa;
//};
////继承后父类的Person的成员（成员函数+成员变量）都会变成子类的一部分。这里体现出了Student和Teacher复用了Person的成员。
////下面我们使用监视窗口查看Student和Teacher对象，可以看到变量的复用。调用Print可以看到成员函数的复用。
//struct Student:public Person
//{
//public:
//	void F()
//	{
//		Print();
//	}
//protected:
//	int _stuid;//学号
//};
//class Teacher : public Person
//{
//protected:
//	int _jobid; // 工号
//};
//
//int main()
//{
//	Student s;
//	//s._aa = 1; // 不可见
//
//	Teacher t;
//	//s.Print();
//	t.Print();
//
//	return 0;
//}

//切片
//class Person
//{
//protected:
//	string _name; // 姓名
//	string _sex;  // 性别
//public:
//	void f()
//	{
//		_age = 1;
//	}
//private:
//	int	_age;	 // 年龄
//};
//
//class Student : public Person
//{
//public:
//	/*void f()
//	{
//	_age = 1;
//	}*/
//public:
//	int _No; // 学号
//};
//
//int main()
//{
//	Person p;
//	Student s;
//
//	// 父类=子类赋值兼容 -》 切割  切片
//	// 这里不存在类型转换，是语法天然支持行为
//	p = s;
//	Person* ptr = &s;
//	ptr->f();
//	//ptr->_age = 1;
//	Person& ref = s;
//
//	// 子类 = 父类
//	//s = (Student)p; // 怎样都不行
//	Student* pptr = (Student*)&p;
//	Student& rref = (Student&)p;
//	// 很危险的，存在越界访问的风险
//	//pptr->_No = 1;
//
//
//	// 类型转换，中间会产生临时变量
//	int i = 1;
//	double d = 2.2;
//	i = d;
//	const int& ri = d;
//
//	return 0;
//}


//隐藏
//int a = 0;
//// 1：A和B的func构造函数重载
//// 2：编译报错
//// 3：运行报错
//// 4：A和B的func构造函数隐藏
//class A
//{
//public:
//	void fun()
//	{
//		cout << "func()" << endl;
//	}
//};
//
//class B : public A
//{
//public:
//	void fun(int i)
//	{
//		cout << "func(int i)->" << i << endl;
//	}
//};
//
//void Test()
//{
//	B b;
//	b.fun(1);
//	//b.fun(); // 被隐藏了，所以调不动
//	b.A::fun();
//	A a;
//	a = b;
//	a.fun();
//};
//
//int main()
//{
//	int a = 1;
//	cout << ::a << endl;
//	Test();
//	return 0;
//}

//基类默认构造函数
//class Person
//{
//public:
//	Person(const char* name)
//		: _name(name)
//	{
//		cout << "Person()" << endl;
//	}
//
//	Person(const Person& p)
//		: _name(p._name)
//	{
//		cout << "Person(const Person& p)" << endl;
//	}
//
//	Person& operator=(const Person& p)
//	{
//		cout << "Person operator=(const Person& p)" << endl;
//		if (this != &p)
//			_name = p._name;
//
//		return *this;
//	}
//
//	~Person()
//	{
//		cout << "~Person()" << endl;
//		//delete[] _ptr;
//	}
//protected:
//	string _name; // 姓名
//	//int* _ptr = new int[10];
//};
//
//class Student : public Person
//{
//public:
//	Student(const char* name = "张三", int num = 1)
//		:Person(name)
//		, _num(num)
//	{}
//
//	// s2(s1)
//	Student(const Student& s)
//		:Person(s)
//		, _num(s._num)
//	{}
//
//	// s2 = s1
//	Student& operator=(const Student& s)
//	{
//		if (this != &s)
//		{
//			Person::operator=(s);
//
//			_num = s._num;
//		}
//
//		return *this;
//	}
////
////	// 析构函数名字会被统一处理成destructor()。（ps：至于为什么会统一处理，多态章节会讲解）
////	// 那么子类的析构函数跟父类的析构函数就构成隐藏
//	~Student()
//	{
//		//Person::~Person();
//		// delete[] _ptr;
//	}
////	// 子类析构函数结束时，会自动调用父类的析构函数
////	// 所以我们自己实现子类析构函数时，不需要显示调用父类析构函数
////	// 这样才能保证先析构子类成员，再析构父类成员
////
//protected:
//	int _num = 1; //学号
//	//string _s = "hello world1111111111111111";
//	//int* _ptr = new int[10];
//};
////
////// 1、派生类的重点的四个默认成员函数，我们不写，编译器会默认生成的会干些什么事情呢！
////// 2、如果我们要写，要做些什么事情呢？
////
////// 我们不写默认生成的派生的构造和析构？ 
////// a、父类继承下来得 (调用父类默认构造和析构处理)  b、自己的（内置类型和自定义类型成员）(跟普通类一样)
////// 我们不写默认生成的拷贝构造和operator=？
////// a、父类继承下来得 (调用父类拷贝构造和operator=) b、自己的（内置类型和自定义类型成员）(跟普通类一样)
////
////// 总结：原则，继承下来调用父类处理，自己的按普通类基本规则。
////
////// 什么情况下必须自己写？
////// 1、父类没有默认构造，需要我们自己显示写构造
////// 2、如果子类有资源需要释放，就需要自己显示写析构
////// 3、如果子类存在浅拷贝问题，就需要自己实现拷贝构造和赋值解决浅拷贝问题
////
////// 如果我们要自己写怎么办？如何自己写？
////// 父类成员调用父类的对应构造、拷贝构造、operator=和析构处理
////// 自己成员按普通类处理。
////
//int main()
//{
//	Student s1;
//	Student s2(s1);
//	//Student s3("jack", 18);
//
//	//s1 = s3;
//
//	return 0;
//}

//虚继承
//class Person
//{
//public:
//	string _name; // 姓名
//	int _a[10000];
//};
//
//class Student : public Person
//{
//public:
//	int _num; //学号
//};
//
//class Teacher : public Person
//{
//public:
//	int _id; // 职工编号
//};
//
//class Assistant : public Student, public Teacher
//{
//protected:
//	string _majorCourse; // 主修课程
//};
//
//int main()
//{
//	// 二义性、数据冗余
//	Assistant a;
//	a._id = 1;
//	a._num = 2;
//	a.Student::_name = "小张";
//	a.Teacher::_name = "张老师";
//
//	cout << sizeof(a) << endl;
//
//	return 0;
//}

class A
{
public:
	int _a;
};

//class B : public A
class B : virtual public A
{
public:
	virtual  void funcB()
	{
		cout << "void funcB()" << endl;
	}

	int _b;
};

//class C : public A
class C : virtual public A
{
public:
	virtual  void funcC()
	{
		cout << "void funcC()" << endl;
	}
	int _c;
};

class D : public B, public C
{
public:
	int _d;
};

int main()
{
	D d;
	d.B::_a = 1;
	d.C::_a = 2;
	d._b = 3;
	d._c = 4;
	d._d = 5;
	d._a = 0;

	return 0;
}