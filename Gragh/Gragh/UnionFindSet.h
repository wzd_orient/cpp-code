#pragma once
#include<vector>
#include <utility> 

//并查集的基本实现思想
class UnionFindSet
{
public:
	UnionFindSet(int size)
		:_set(size,-1)
	{}

	size_t FindRoot(int x)
	{
		//没有实现路径压缩，存在很多层的，再查找的时候很不方便，复杂度比较高
		/*while(_set[x] >= 0)
			x = _set[x];*/

		//实现在每次查找的时候都压缩一次路径，先找到路径的根
		int root = x;
		while (_set[root] >= 0)
		{
			root = _set[root];
		}

		//实现路径压缩
		while (_set[x] >= 0)
		{
			int parent = _set[x];
			_set[x] = root;
			x = parent;
		}
		return root;
	}
	void Union(int x1,int x2)
	{
		int root1 = FindRoot(x1);
		int root2 = FindRoot(x2);
		
		//控制数据量小的向数据量大的集合合并
		if (abs(_set[root1]) < abs(_set[root2]))
			std::swap(root1, root2);

		if (root1 != root2)
		{
			_set[root1] += _set[root2];
			_set[root2] = root1;
		}
	}
	bool Inset(int x1, int x2)
	{
		return FindRoot(x1) == FindRoot(x2);
	}
	size_t SetCount()
	{
		size_t count = 0;
		for (size_t i = 0; i < _set.size(); i++)
		{
			if (_set[i] < 0)
			{
				++count;
			}
		}
		return count;
	}
private:
	std::vector<int> _set;
};
void TestUFS()
{
	UnionFindSet u(10);

	u.Union(0, 6);
	u.Union(7, 6);
	u.Union(7, 8);

	u.Union(1, 4);
	u.Union(4, 9);

	u.Union(2, 3);
	u.Union(2, 5);

	std::cout << u.SetCount() << std::endl;
}