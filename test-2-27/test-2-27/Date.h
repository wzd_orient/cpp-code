#pragma once
#include<iostream>
using namespace std;
class Date
{
	//友元函数
	friend ostream& operator<<(ostream& out, const Date& d);
public:
	Date(int year = 0, int month = 1, int day = 1);
	void Print();
	void PrintWeekDay() const;
	int GetMonthDay(int year,int month);
	bool operator>=(const Date& d) const;
	bool operator<(const Date& d) const;
	Date& operator=(const Date& d);
	bool operator>(const Date& d) const;
	bool operator==(const Date& d) const;
	bool operator!=(const Date& d)const;

	Date& operator+=(int day);
	Date operator+(int day);
	Date& operator-=(int day);
	Date operator-(int day);
	int operator-(const Date& d) const;

	
	// ++d1;
	Date& operator++();

	// d1++; 后置为了跟前置++，进行区分
	// 增加一下参数占位，跟前置++,构成函数重载
	Date operator++(int);
private:
	int _year;
	int _month;
	int _day;
};
ostream& operator<<(ostream& out, const Date& d);