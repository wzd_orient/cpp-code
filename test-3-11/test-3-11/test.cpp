#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
using namespace std;
//c++类和对象下篇
//class A
//{
//public:
//	A(int a = 0)
//	{
//		int _a = a;
//	}
//private:
//	int _a;
//};
//class Date
//{
//public:
//	/*Date(int year, int month, int day)
//	{
//		int _year = year;
//		int _month = month;
//		int _day = day;
//	}*/
//	Date(int year, int month, int day,int i)
//		:_year(year)
//		, _month(month)
//		, _day(day)
//		, _N(10)
//		, _ref(i)
//	{
//	}
//	
//private:
//	int _year;
//	int _month;
//	int _day;
//	const int _N;//const
//	int& _ref;//引用
//	A _aa;//没有默认构造的自定义类型成员变量必须在初始化列表初始化，
//	//总结：
//	//1.初始化列表 - 成员变量定义的地方
//	//2.const ，引用，没有默认构造函数的自定义类型成员变量必须在初始化列表初始化
//	//因为他们必须在定义时初始化
//	//3.对于像其他类型成员变量，如int _year,int _month 在哪里初始化都可以。
//};
//class Date
//{
//public:
//	Date(int year)
//		:_year(year)
//	{}
//	Date(const Date& d)
//	{
//		cout << "Date(const Date& d)" << endl;
//	}
//	explicit Date(int year)
//		:_year(year)
//	{
//		cout << "explicit Date(int year)" << endl;
//	}
//private:
//	int _year;
//};
//int main()
//{
//	//int i = 0;
//	//Date d1(2022, 1, 19, i);
//	Date d1(2022);
//	Date d2 = 2022;//隐式类型转化
//	//本来用2022构造一个临时对象Date(2022),再用这个对象拷贝构造对象
//	//本来c++的编译器是一个连续的过程由于c++的优化拷贝构造和构造合二为一
//	//所有这里只有一个构造，而explict关键字会禁止隐式类型转化
//	return 0;
//}
//static成员
//class A
//{
//public:
//	A(int a = 0)
//		:_a(a)
//	{
//		++_Scount;
//	}
//	A(const A&aa)
//		:_a(aa._a)
//	{
//		++_Scount;
//	}
//	//没有this指针，只能访问成员变量和成员函数
//	static int GetScount()
//	{
//		return _Scount;
//	}
//private:
//	int _a;
//	static int _Scount;//声明
//	//静态成员变量属于整个类，所有对象，生命周期在整个程序运行周期
//	//类成员函数中，可以随便访问
//};
//int A::_Scount = 0;//定义初始化
//int main()
//{
//	A a1;
//	cout << A::GetScount() << endl;
//	//或者把_Scount放着public中通过A::_Scount直接访问
//	return 0;
//}
//class A
//{
//public:
//	A(int a = 0)
//	{
//		_a = a;
//	}
//private:
//	int _a;
//};
//class Date
//{
//public:
//	Date(int year)
//		:_year(year)
//	{}
//private:
//	//注意：这里给的都是缺省值，不是初始化
//	int _year = 0;
//	const int _N = 0;
//	int*p = (int*)malloc(sizeof(int));
//	A a = 10;
//	static int b;
//};
//int Date::b = 10;
//int main()
//{
//	return 0;
//}
//class Date
//{
//friend void Print(Date& d);
//public:
//	Date(int year, int month, int day)
//		:_year(year)
//		, _month(month)
//		, _day(day)
//	{
//	}
//
//private:
//	int _year;
//	int _month;
//	int _day;
//};
//void Print(Date& d)
//{
//	cout << d._year << "-" << d._month << "-" << d._day << endl;
//}
//int main()
//{
//	return 0;
//}
//class Time
//{
//	friend class Date;   // 声明日期类为时间类的友元类，则在日期类中就直接访问Time类中的私有成员变量
//public:
//	Time(int hour = 0, int minute = 0, int second = 0)
//		: _hour(hour)
//		, _minute(minute)
//		, _second(second)
//	{}
//
//	void f(Date d);
//
//private:
//	int _hour;
//	int _minute;
//	int _second;
//};
//
//class Date
//{
//public:
//	Date(int year = 1900, int month = 1, int day = 1)
//		: _year(year)
//		, _month(month)
//		, _day(day)
//	{
//		_t._hour = 0;
//		_t._minute = 0;
//		_t._second = 0;
//	}
//
//	void SetTimeOfDate(int hour, int minute, int second)
//	{
//		// 直接访问时间类私有的成员变量
//		_t._hour = hour;
//		_t._minute = minute;
//		_t._second = second;
//	}
//
//private:
//	int _year;
//	int _month;
//	int _day;
//	Time _t;
//};
//int main()
//{
//	return 0;
//}
class A
{
private:
	static int k;
	int h;
public:
	// 内部类
	// 1、内部类B和在全局定义是基本一样的，只是他的受外部类A类域限制,定义在A的类域中
	// 2、内部类B天生就是外部类A的友元，也就是B中可以访问A的私有，A不能访问B的私有
	class B
	{
		// friend class A;
	public:
		void foo(const A& a)
		{
			cout << k << endl;//OK
			cout << a.h << endl;//OK
		}

	private:
		int _b;
	};

	void f(B bb)
	{
		// A不是B的友元，不能访问B
		bb._b;
	}
};