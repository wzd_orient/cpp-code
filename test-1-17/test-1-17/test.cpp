#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<assert.h>
using namespace std;
//class Date
//{
//public:
//	//构造函数
//	Date(int a = 0, int b = 0, int c = 0)
//	{
//		_year = a;
//		_month = b;
//		_day = c;
//	}
//	//析构函数-程序结束是清理成员变量
//	~Date()
//	{
//		 
//	}
//	void print()
//	{
//		cout << _year << "-"<< _month << "-" << _day << endl;
//	}
//	Date operator-()
//	{
//
//	}
//private:
//	int _year;
//	int _month;
//	int _day;
//};
//int main()
//{
//	Date d1(2022,1,17);
//	d1.print();
//	return 0;
//}
//struct/class
//c++兼容c里面的结构体
//同时struct在c++中也升级成了类
//c++类跟结构体不同处理可以定义变量，还是可以定义方法/函数
//struct Student
//{
//	//成员变量
//	char _name[10];
//	int _age;
//	int _id;
//	//成员函数
//	void Init(const char* name, int age, int id)
//	{
//		strcpy(_name, name);
//		_age = age;
//		_id = id;
//	}
//	void Print()
//	{
//		cout << _name << endl;
//		cout << _age << endl;
//		cout << _id << endl;
//	}
//};
// 面向对象三大特性：封装、继承、多态
// 封装: 1、数据和方法都放到了一起在类里面 2、访问限定符
// 一般在定义类的时候，建议明确定义访问限定符，不要用class/struct默认限定
//class Student
//{
//private:
//	//成员变量
//	char _name[10];
//	int _age;
//	int _id;
//public:
//	//成员函数
//	void Init(const char* name, int age, int id)
//	{
//		strcpy(_name, name);
//		_age = age;
//		_id = id;
//	}
//	void Print()
//	{
//		cout << _name << endl;
//		cout << _age << endl;
//		cout << _id << endl;
//	}
//};
//class Stack
//{
//public:
//	void Init()
//	{
//		_a = nullptr;
//		_top = _capacity = 0;
//	}
//	void Push(int x)
//	{
//
//	}
//	int Top()
//	{
//		assert(_top > 0);
//		return _a[_top - 1];
//	}
//private:
//	int* _a;
//	int _top;
//	int _capacity;
//
//};
////类中仅仅有成员函数
//class A2
//{
//public:
//	void f2()
//	{
//	}
//	//类中什么也没有的话，是一个空类大小是1
// // 空类会给1byte，这1byte不存储有效数据，只是为了占位，表示对象存在
//};
//int main()
//{
//	//struct Student s1;//兼容c
//	//Student s2;//升级到类，student类名，也是类型
//	//s1.Init("张三", 18, 1);
//	//s2.Init("李四", 19, 2);
//	//s1.Print();
//	//s2.Print();
//	Stack st;
//	st.Init();
//	/*st.Push(1);
//	st.Push(2);
//	int top = st.Top();*/
//	// 结论：计算类或者类对象大小，只看成员变量，考虑内存对齐，C++内存对齐规则跟c结构体一致
//	cout << sizeof(Stack) << endl;
//	cout << sizeof(st) << endl;
//	/*A2 aa;
//	A2 bb;
//	cout << &aa << endl;
//	cout << &bb << endl;*/
//	return 0;
//}
class Date
{
public:
	void Display()
	{
		cout << _year << "-" << _month << "-" << _day << endl;
	}
	void SetDate(int year, int month, int day)
	{
		_year = year;
		_month = month;
		_day = day;
	}
private:
	int _year; // 年
	int _month; // 月
	int _day; // 日
};
int main()
{
	Date d1, d2;
	d1.SetDate(2018, 5, 1);
	d2.SetDate(2018, 7, 1);
	d1.Display();
	d2.Display();
	return 0;
}
