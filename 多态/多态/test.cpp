#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
using namespace std;

//多态：多种形态
//静态的多态：函数重载，看起来是调用一个函数不同的行为，静态，原理是编译是实现
//动态的多态:一个父类的引用或者指针去调用同一个函数，传递不同的对象，会调用不同的函数
//动态：原理运行时实现
//本质:不同人去做同一件事情，结果不一样
//class Person
//{
//public:
//	virtual void BuyTicket()
//	{
//		cout << "票价 - 全价" << endl;
//		return;
//	}
//};
//class Student : public Person
//{
//public:
//	//子类中满足三同（函数名，参数，返回值）虚函数，叫做重写（覆盖）
//	//virtual void BuyTicket()
//	void BuyTicket()
//	{
//		cout << "票价 - 半价" << endl;
//	}
//};
////构成多态，跟p的类型没有关系，传的那个类型对象，调用的就是这个类型的函数
////----和对象有关系
////不构成多态，调用的就是p类型的函数 --- 跟类型有关
//void Func(Person& p)
//{
//	p.BuyTicket();
//}
//int main()
//{
//	//int i = 1;
//	//double d = 2.22;
//	//cout << i;//调用的是cout.operator<<(int)
//	//cout << d;
//
//	Person ps;
//	Student st;
//
//	
//	//Func(ps);
//	//Func(st);
//	//st.BuyTicket();
//	st.Person::BuyTicket();
//	return 0;
//}

//重写要求返回值相同还有一个例外，协变 -- 要求返回值时父类关系的指针或者引用
//class A{};
//class B : public A{};
//class Person
//{
//public:
//	/*virtual A* BuyTicket()
//	{
//		cout << "买票 -- 半价" << endl;
//		return nullptr;
//	}*/
//	virtual Person* BuyTicket()
//	{
//		cout << "买票 -- 全价" << endl;
//		return nullptr;
//	}
//};
//class Student : public Person
//{
//public:
//	// 子类中满足三同(函数名、参数、返回值)虚函数，叫做重写(覆盖)
//	/*virtual B* BuyTicket() 
//	{ 
//		cout << "买票-半价" << endl; 
//		return nullptr; 
//	}*/
//	virtual Student* BuyTicket() 
//	{
//		cout << "买票-半价" << endl;
//		return nullptr;
//	}
//};
//void Func(Person& p)
//{
//	p.BuyTicket();
//}
// 析构函数是虚函数，是否构成重写？-- 构成
// 析构函数名被特殊处理了，处理成了destructor
//class Person {
//public:
//	~Person() 
//	{ 
//		cout << "~Person()" << endl;
//	}
//};
//
//class Student : public Person {
//public:
//	~Student()
//	{ 
//		cout << "~Student()" << endl;
//	}
//};
//int main()
//{
//	// 普通对象，析构函数是否虚函数,是否完成重写，都正确调用了
//	 /*Person p;
//	 Student s;*/
//	
//	// 动态申请的对象，如果给了父类指针管理，那么需要析构函数是虚函数
//	//Person* p1 = new Person; // operator new + 构造函数
//	//Person* p2 = new Student;
//	
//	
//	// 析构函数 + operator delete
//	//p1->destructor()   
//	//delete p1; 
//	delete p2;
//	// p2->destructor()
//	return 0;
//}

//总结:
// 虚函数的重写允许，两个都是虚函数或者父类是虚函数，再满足三同，就构成重写。
// 其实这个是C++不是很规范的地方，当然我们建议两个都写上virtual
// 本质上，子类重写的虚函数，可以不加virtual是因为析构函数，大佬设计初衷
// 父类析构函数加上virtual，那么就不存在不构成太态，没调用子类析构函数，内存泄漏场景
// 建议，我们自己写的时候，都加上virtual，肯定没毛病
//class Car
//{
//public:
//	virtual void Drive() = 0;
//};
//class Benz :public Car
//{
//public:
//	virtual void Drive()
//	{
//		cout << "Benz-舒适" << endl;
//	}
//};
//int main()
//{
//	Car* c = new Benz;
//	return 0;
//}


//多态的原理
//class Base
//{
//public:
//	virtual void fun1()
//	{
//		cout << "void fun1()" << endl;
//	}
//	virtual void fun2()
//	{
//		cout << "void fun2()" << endl;
//	}
//private:
//	int _b = 1;
//	char _ch = 'A';
//};
//
//int main()
//{
//	cout << sizeof(Base) << endl;
//	Base bb;
//	return 0;
//}


//class Person {
//public:
//	virtual void BuyTicket()
//	{ cout << "买票-全价" << endl; }
//
//	void f()
//	{
//		cout << "f()" << endl;
//	}
//protected:
//	int _a = 0;
//};
//
//class Student : public Person {
//public:
//	virtual void BuyTicket()
//	{ cout << "买票-半价" << endl; }
//protected:
//	int _b = 0;
//};
//
//void Func(Person& p)
//{
//	// 多态调用，在编译时，不能确定调用的是哪个函数
//	// 运行时，去p指向对象的虚表中
//	// 找到虚函数的地址。
//	p.BuyTicket();
//	p.f();
//}
//
//// 普通函数和虚函数存储的位置是否一样？
//// 他们是一样的，都在代码段。只是虚函数要把地址存一到虚表，方便实现多态
//int main()
//{
//	Person Mike;
//	Func(Mike);
//
//	Student Johnson;
//	Func(Johnson);
//
//	//Person& r1 = Johnson;
//	//Person p = Johnson;
//
//	//Person p1 = Mike;
//	//Person p2 = Johnson;
//	//// 不是多态，编译时确定地址
//	//p1.BuyTicket();
//	//p2.BuyTicket();
//
//	//Person p1;
//	//Person p2;
//
//	//Student s1;
//	//Student s2;
//
//	return 0;
//}


typedef void(*VF_PTR)();

//void PrintVFTable(VF_PTR table[])
// 打印虚函数表中内容
void PrintVFTable(VF_PTR* table)
{
	for (int i = 0; table[i] != nullptr; ++i)
	{
		printf("vft[%d]:%p->", i, table[i]);
		VF_PTR f = table[i];
		f();
	}
	cout << endl << endl;
}


//class Base
//{
//public:
//	virtual void Func1()
//	{
//		cout << "Base::Func1()" << endl;
//	}
//
//	virtual void Func2()
//	{
//		cout << "Base::Func2()" << endl;
//	}
//
//	void Func3()
//	{
//		cout << "Base::Func3()" << endl;
//	}
//
//private:
//	int _b = 1;
//};
//
//class Derive : public Base
//{
//public:
//	virtual void Func1()
//	{
//		cout << "Derive::Func1()" << endl;
//	}
//
//	virtual void Func4()
//	{
//		cout << "Derive::Func4()" << endl;
//	}
//private:
//	int _d = 2;
//};
//
////#ifdef _WIN64
////	PrintVFTable((VF_PTR*)(*(long long*)&b));
////#else
////	PrintVFTable((VF_PTR*)(*(int*)&b));
////#endif // _WIN64
//int main()
//{
//	Base b;
//
//	PrintVFTable((VF_PTR*)(*(void**)&b));
//
//	Derive d;
//	PrintVFTable((VF_PTR*)(*(void**)&d));
//	/*Derive d;
//
//	Base* p1 = &b;
//	p1->Func1();
//*/
//	//p1 = &d;
//	//p1->Func1();
//
//
//	return 0;
//}

class Base1 {
public:
	virtual void func1() { cout << "Base1::func1" << endl; }
	virtual void func2() { cout << "Base1::func2" << endl; }
private:
	int b1;
};

class Base2 {
public:
	virtual void func1() { cout << "Base2::func1" << endl; }
	virtual void func2() { cout << "Base2::func2" << endl; }
private:
	int b2;
};

class Derive : public Base1, public Base2 {
public:
	virtual void func1() { cout << "Derive::func1" << endl; }
	virtual void func3() { cout << "Derive::func3" << endl; }
private:
	int d1;
};
int main()
{
	Derive der;
	PrintVFTable((VF_PTR*)(*(void**)&der));
	return 0;
}


