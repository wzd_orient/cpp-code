#pragma once
#include<iostream>
#include<string>
#include<vector>
#include<algorithm>
using namespace std;


void test_vector1()
{
	vector<int> v1;
	vector<int> v2(10, 8);
	vector<int> v3(++v2.begin(), --v2.end());
	//cout << v3 << endl;
	vector<int> v4(v3);
	for (auto e : v4)
	{
		cout << e;
	}
	cout << endl;
	string s("hello world");
	vector<char> v5(s.begin(), s.end());
	for(auto e:v5)
	{
		cout << e;
	}
}
void test_vector2()
{
	vector<int> v;
	v.push_back(1);
	v.push_back(2);
	v.push_back(3);
	v.push_back(4);
	
	//遍历
	//下标
	for (size_t i = 0; i < v.size(); i++)
	{
		v[i] += 1;
		cout << v[i] << " ";
	}
	cout << endl;

	//
	vector<int>::iterator it = v.begin();
	while (it != v.end())
	{
		*it -= 1;
		cout << *it << " ";
		++it;
	}
	cout << endl;

	for (auto e : v)
	{
		cout << e << " ";
	}
	cout << endl;

}
void test_vector3()
{
	vector<int> v;
	v.push_back(1);
	v.push_back(2);
	v.push_back(3);
	v.push_back(4);
	cout << v.max_size() << endl;
	//返回vector能存储的最多元素的大小

	v.reserve(100);//扩容
	v.resize(100, 5);//扩容 +初始化 或者 删除数据
	v.resize(2);

}
void test_vector4()
{
	vector<int> v;
	v.push_back(1);
	v.push_back(2);
	v.push_back(3);
	v.push_back(4);

	//v.assign(10, 5);
	//覆盖原来的所有值
	//for (auto e : v)
	//{
	//	cout << e << " ";
	//}
	//cout << endl;
	vector<int>::iterator ret = find(v.begin(), v.end(), 2);
	if (ret != v.end())
	{
		cout << "找到了" << endl;
		v.insert(ret, 30);
		//v.erase(ret) 
		//不能再删除了因为ret失效了，这个是属于迭代器失效的问题，后面我们将会讲解

	}
	v.insert(v.begin(), -1);
	for (auto e : v)
	{
		cout << e << " ";
	}

	v.clear();

	for (auto e : v)
	{
		cout << e << " ";
		cout << "数组是空的了";
	}
}
//#include <iostream>
//#include <vector>
//
//int main()
//{
//	size_t sz;
//	std::vector<int> foo;
//	sz = foo.capacity();
//	std::cout << "making foo grow:\n";
//	for (int i = 0; i < 100; ++i) {
//		foo.push_back(i);
//		if (sz != foo.capacity()) {
//			sz = foo.capacity();
//			std::cout << "capacity changed: " << sz << '\n';
//		}
//	}
//}
