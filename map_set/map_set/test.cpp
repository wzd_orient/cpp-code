#define _CRT_SECURE_NO_WARNINGS 1

#include<iostream>
#include<set>
#include<stdio.h>
#include<time.h>
#include<string>
#include<map>
#include<vector>
#include <functional>
#include<algorithm>
#include<queue>
using namespace std;
#include<map>

#include"AVLTree.h"
#include"RBTree.h"
#include"MyMap.h"
#include"MySet.h"
void test_set1()
{
	//排序 + 去重
	set<int> s;
	s.insert(3);
	s.insert(1);
	s.insert(3);
	s.insert(5);
	s.insert(8);
	s.insert(10);
	set<int>::iterator it = s.begin();
	//遍历的方式是中序遍历
	while (it != s.end())
	{
		cout << *it << " ";
		++it;
	}
	cout << endl;

	set<int>::iterator pos = s.find(5);
	if (pos != s.end())
	{
		//pos必须是一个有效的迭代器
		s.erase(pos);
	}
	cout << s.erase(1) << endl;
	cout << s.erase(30) << endl;

	for (auto e : s)
	{
		cout << e << " ";
	}
	cout << endl;

}
void test_multiset()
{
	//排序
	multiset<int> s;
	s.insert(3);
	s.insert(1);
	s.insert(1);
	s.insert(5);
	s.insert(1);
	s.insert(8);
	s.insert(10);
	s.insert(12);
	
	auto it = s.begin();
	while (it != s.end())
	{
		cout << (*it) << " ";
		++it;
	}
	cout << endl;
	
	//这里需要注意的是mult、iset的作用没有去重那么find返回的是哪一个节点的迭代器呢？
	//返回中序第一个val值所在的节点的迭代器
	multiset<int>::iterator pos = s.find(8);
	while (pos != s.end())
	{
		cout << (*pos) << " ";
		++pos;
	}
	cout << endl;

	pos = s.find(1);
	/*while (pos != s.end() && *pos == 1)
	{
		auto next = pos;
		++next;
		s.erase(pos);
		pos = next;
	}*/
	//erase（值）会删除所有你需要删除的需要删除的节点，返回值是你删除节点的个数
	cout << s.erase(1) << endl;

	for (auto e : s)
	{
		cout << e  << " ";
	}
	cout << endl;

	pos = s.find(5);
	if (pos != s.end())
	{
		//不允许修改
		//*pos += 10;
	}


}
void test_map1()
{
	map<string, string> dict;
	pair<string, string> kv1("sort", "排序");
	dict.insert(kv1);
	dict.insert(pair<string, string>("string", "字符串"));
	//自动推到类型 --- c++11的内容
	dict.insert(make_pair("test", "测试"));

	map<string, string>::iterator it = dict.begin();
	while (it != dict.end())
	{
		//it->first = "wess";
		//it->second = "目击";
		//cout << (*it).first << endl;
		cout << it->first << ":" << it->second << endl;
		++it;
	}
	cout << endl;
	for (auto& kv : dict)
	{
		cout << kv.first << ":" << kv.second << endl;
	}
	cout << endl;
}
void test_map2()
{
	string arr[] = { "苹果", "苹果", "香蕉", "苹果", "樱桃" };
	map<string,int> CountMap;
	for (auto& str : arr)
	{
		auto ret = CountMap.find(str);
		if (ret == CountMap.end())
		{
			CountMap.insert(make_pair(str, 1));
		}
		else
		{
			ret->second++;
		}
	}
	for (auto& str : arr)
	{
		auto kv = CountMap.insert(make_pair(str, 1));
		if (kv.second == false)
		{
			kv.first->second++;
		}
	}
	for (auto& str : arr)
	{
		CountMap[str]++;
	}
	for (auto& kv : CountMap)
	{
	cout << kv.first << ":" << kv.second << endl;
	}
	cout << endl;

}
void test_map3()
{
	map<string, string> dict;
	dict.insert(make_pair("sort", "排序"));
	dict.insert(make_pair("left", "左边"));
	dict.insert(make_pair("right", "右边"));
	dict["left"] = "剩余";//修改
	dict["test"];//插入
	//cout << dict["sort"] << endl;//查找
	dict["string"] = "字符串";//插入 + 修改
	
	for (auto& e : dict)
	{
		cout << e.first << ":" << e.second << endl;
	}
}
void test_map4()
{
	multimap<string, string> dict;
	dict.insert(make_pair("sort", "排序"));
	dict.insert(make_pair("left", "左边"));
	dict.insert(make_pair("left", "剩余"));
	dict.insert(make_pair("left", "遗留"));

	auto e = dict.find("left");
	cout << (*e).first << ":" << (*e).second << endl;
	cout << dict.count("left") << endl;

}
struct CountVal
{
	bool operator()(const pair<string, int>& l, const pair<string, int>& r)
	{
		return l.second < r.second;//大于是降序小于是升序
	}
};
struct CountIteraVal
{
	bool operator()(const map<string, int>::iterator& l, const map<string, int>::iterator& r)
	{
		return l->second < r->second;//大于是降序小于是升序
	}
};
void GetFavoriteFruit(vector<string>& v, int K)
{
	map<string, int> CountMap;
	for (auto e : v)
	{
		CountMap[e]++;
	}
	//将map的元素放入SortV当中进行快速排序
	/*vector<pair<string, int>> SortV;
	for (auto e : CountMap)
	{
		SortV.push_back(e);
	}
	sort(SortV.begin(), SortV.end(), CountVal());*/
	/*vector<map<string, int>::iterator> SortV;
	auto it = CountMap.begin();
	while (it != CountMap.end())
	{
		SortV.push_back(it);
		++it;
	}
	sort(SortV.begin(), SortV.end(), CountIteraVal());
	for (int i = 0; i < K; i++)
	{
		cout << SortV[i]->first << ":" << SortV[i]->second << endl;
	}*/
	//multimap<int, string,greater<int>> SortMap;
	//for (auto& e : CountMap)
	//{
	//	SortMap.insert(make_pair(e.second, e.first));
	//}
	//
	//for (auto & kv : SortMap)
	//{
	//	cout << kv.second << ":" << kv.first << endl;
	//}
	//cout << endl;
	//auto it = SortMap.begin();
	//for (int i = 0; i < K; i++)
	//{
	//	cout << it->second << ":" << it->first << endl;
	//	++it;
	//}
	///////////////////////////////////堆////////////////////////////////
	//priority_queue<pair<string, int>, vector<pair<string, int>>, CountVal> pq;
	//for (auto& kv : CountMap)
	//{
	//	pq.push(kv);
	//}
	//for (int i = 0; i < K; i++)
	//{
	//	cout << pq.top().first << ":" << pq.top().second << endl;
	//	pq.pop();
	//}

	priority_queue<map<string, int>::iterator, vector<map<string, int>::iterator>, CountIteraVal> pq;
	auto it = CountMap.begin();
	while (it != CountMap.end())
	{
		pq.push(it);
		++it;
	}

	while (K--)
	{
		cout << pq.top()->first << ":" << pq.top()->second << endl;
		pq.pop();
	}
	cout << endl;
}
int main()
{
	//test_set1();
	//test_multiset();
	//test_map1();
	//test_map2();
	//test_map3();
	//test_map4();
	//vector<string> v = { "苹果", "苹果", "香蕉", "苹果", "香蕉", "苹果", "樱桃", "哈密瓜", "榴莲", "榴莲", "苹果" };
	//GetFavoriteFruit(v, 3);
	//TestAVLTree();
	//TestRBTree();

	//wzd::test_map();
	//wzd::test_set();
	return 0;
}