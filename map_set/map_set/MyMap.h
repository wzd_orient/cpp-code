#define _CRT_SECURE_NO_WARNINGS 1
#pragma once
#include"RBTree.h"
namespace wzd
{
	template<class K,class V>
	class map
	{
	public:
		struct MapKeyofT
		{
			const K& operator()(const pair<K, V>& kv)
			{
				return kv.first;
			}
		};

		typedef typename RBTree<K, pair<K, V>, MapKeyofT>::iterator iterator;

		iterator begin()
		{
			return _t.begin();
		}

		iterator end()
		{
			return _t.end();
		}

		pair<iterator,bool> insert(const pair<K, V>& kv)
		{
			return _t.Insert(kv);
		}

		iterator Find(const K& key)
		{
			return _t.Find(key);
		}
		V& operator[](const K& key)
		{
			auto ret = _t.Insert(make_pair(key, V()));
			return ret.first->second;
		}
	private:
		RBTree<K, pair<K, V>, MapKeyofT> _t;
	};

	void test_map()
	{
		map<string, string> dict;
		dict.insert(make_pair("sort", "����"));
		dict.insert(make_pair("string", "�ַ���"));
		dict.insert(make_pair("map", "��ͼ"));

		auto it = dict.begin();
		while (it != dict.end())
		{
			cout << it->first << ":" << it->second << endl;
			//it.operator->()
			++it;
		}
		cout << endl;
	}
}