#define _CRT_SECURE_NO_WARNINGS 1
#include"vector.h"
#include<iostream>
#include<vector>
//模板进阶
using namespace std;
//#define N 100
//非类型模板参数 -- 整形常量
//template<class T,size_t N = 10>
//class Mystcak
//{
//public:
//	void push(const T& x)
//	{}
//private:
//	T _a[N];
//	size_t _top;
//};
//
//template<size_t N>
////template<size_t N,double D,string S>
////浮点数，类都不能作为非类型模板参数
//class A
//{};
//int main()
//{
//	Mystcak<int, 100> st1;
//	Mystcak<int, 200> st2;
//	Mystcak<int> st3;
//	
//	/*vector<int> v;
//	v.resize(10);*/
//
//	return 0;
//}
//函数模板 - 参数匹配
//template<class T>
//bool ObjLess(T left, T right)
//{
//	return left < right;
//}
////bool ObjLess(Date* left,Date* right)
////{
////	return *left < *right;
////}
////函数特化，自动调用Date类型的运算
//class Date
//{
//public:
//	Date(int year = 2001, int month = 1, int day = 1)
//		:_year(year)
//		,_month(month)
//		, _day(day)
//	{}
//	bool operator < (Date d)
//	{
//		return _year < d._year;
//		//此处简介
//	}
//private:
//	int _year;
//	int _month;
//	int _day;
//};
//template<>
//bool ObjLess<Date*>(Date* left, Date* right)
//{
//	return *left < *right;
//}
////int main()
////{
////	cout << ObjLess(1, 2) << endl;
////	Date* p1 = new Date(2001, 1, 2);
////	Date* p2 = new Date(2002, 1, 2);
////	cout << ObjLess(p1, p2) << endl;
////	return 0;
////}
//template<class T1,class T2>
//class Data
//{
//public:
//	Data()
//	{
//		cout << "Date<T1,T2>" << endl;
//	}
//private:
//	T1 _d1;
//	T2 _d2;
//};
////特化 -- 特殊化处理，比如针对某些类型需要特殊化处理
////全特化
//template<>
//class Data<int, char>
//{
//public:
//	Data()
//	{
//		cout << "Date<T1,T2>" << endl;
//	}
//private:
//	int _d1;
//	char _d2;
//};
////偏特化
//template<class T3>
//class Data<T3, char>
//{
//public:
//	Data()
//	{
//		cout << "Data<T3,char>" << endl;
//	}
//private:
//	T3 _d1;
//	char _d2;
//
//};
//template<class T4,class T5>
//class Data<T4*, T5*>
//{
//public:
//	Data()
//	{
//		cout << "Data<T4*, T5*>" << endl;
//	}
//private:
//	T4* _d1;
//	T5* _d2;
//};
//template<class T6, class T7>
//class Data<T6&, T7&>
//{
//public:
//	Data()
//	{
//		cout << "Data<T4*, T5*>" << endl;
//	}
//private:
//	const T6& _d1;
//	const T7& _d2;
//
//};
//template<size_t M>
//class A
//{
//public:
//	A()
//	{
//		cout << "A<M>" << endl;
//	}
//};
//int main()
//{
//	Data<int, int> d1;
//	Data<int, char> d2;
//	Data<double, char> d3;
//	A<20> a;
//	return 0;
//}
int main()
{
	F1(10);
	F2(20);
	return 0;
}